package com.example.sangam.chackravuh;

import android.content.Context;
import android.content.Intent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.internal.StreetViewLifecycleDelegate;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.StringTokenizer;
import java.util.concurrent.ExecutionException;


public class Chakravuh extends AppCompatActivity {

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chakravuh);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Log.d("start123", "started");


        SharedPreferences sharedpreferences = getSharedPreferences("My_Pref_File", Context.MODE_PRIVATE);
        String phone_sh = sharedpreferences.getString("phone",null);
        if (phone_sh != null)
        {
            /*Log.d("My_Phone", phone_sh);
            EditText x = (EditText) findViewById(R.id.phone);
            x.setText(phone_sh);*/
            final Context context = this;
            Intent intent = new Intent(context, CurrentLocation.class);
            startActivity(intent);

        }

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_chakravuh, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    public void toastMsg(String msg) {

        Toast toast = Toast.makeText(this, msg, Toast.LENGTH_LONG);
        toast.show();

    }

    public void GetThis(View view) {
        EditText x = (EditText) findViewById(R.id.phone);
        String str_phone = x.getText().toString();
        Log.d("my_app", str_phone);

        HashMap<String, String> hashMap = new HashMap<String, String>();
        hashMap.put("phone", str_phone);
        URLDataHash mydata = new URLDataHash();
        mydata.url = "192.168.1.105";
        mydata.apicall = "register/otprequest";
        mydata.hashMap = hashMap;
        mydata.port = "8092";
        try {
            JSONObject obj = new nodeHttpRequest(this).execute(mydata).get();
            if(obj != null) {
                Log.d("MYAPP", obj.toString());
                //Goto next page on redirect
                String allowed = "redirect";
                String allowed2 = "success";
                String status = obj.getString("status");
                if (status.equals(allowed)) {
                    // calling new activity here -> Home page(Current Location)

                    JSONObject resp = obj.getJSONObject("resp");
                    String token = resp.getString("token");

                    SharedPreferences sharedpreferences = getSharedPreferences("My_Pref_File", Context.MODE_PRIVATE);
                    Editor editor = sharedpreferences.edit();
                    editor.putString("phone", str_phone);
                    editor.putString("token",token);
                    editor.commit();

                    final Context context = this;
                    Intent intent = new Intent(context, CurrentLocation.class);
                    startActivity(intent);

                }
                else if(status.equals(allowed2)){
                    toastMsg("Mobile Number Verification Initiated...");
                    toastMsg("Please Wait for OTP SMS...!!");

                }
                else {
                    String error = obj.getString("err");
                    toastMsg("Error Message: " + error);
                }

            } else {
                toastMsg("Network Error, Please Try Again");
            }

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    public void GetOtp(View view) {
        EditText z = (EditText) findViewById(R.id.otp);
        String str_otp = z.getText().toString();

        EditText y = (EditText) findViewById(R.id.phone);
        String str_phone = y.getText().toString();


        Log.d("my_otp", str_otp);
        HashMap<String, String> hashMap = new HashMap<String, String>();
        hashMap.put("phone", str_phone);
        hashMap.put("otp", str_otp);

        URLDataHash mydata = new URLDataHash();
        mydata.url = "192.168.1.105";
        mydata.apicall = "register/verify";
        mydata.hashMap = hashMap;
        mydata.port = "8092";
        //Toast message fot OTP verification
        toastMsg("Verifying OTP..");

        try {
            Log.d("my_otp", str_otp);
            JSONObject obj = new nodeHttpRequest(this).execute(mydata).get();
            if (obj != null) {
                Log.d("MYAPP", obj.toString());
                JSONObject resp = obj.getJSONObject("resp");
                String token = resp.getString("token");
                Log.d("Mytoken", token);


                String allowed = "success";
                String status = obj.getString("status");
                if (status.equals(allowed)) {

                    // Storing Data into SharedPref when success received.
                    SharedPreferences sharedpreferences = getSharedPreferences("My_Pref_File", Context.MODE_PRIVATE);
                    Editor editor = sharedpreferences.edit();
                    editor.putString("phone", str_phone);
                    editor.putString("token",token);
                    editor.commit();

                    String phone_sh = sharedpreferences.getString("phone",null);
                    Log.d("My_Phone", phone_sh);
                    String token_sh = sharedpreferences.getString("token",null);
                    Log.d("My_token", token);

                    // calling new activity here -> Home page(Current Location)
                    final Context context = this;
                    Intent intent = new Intent(context, CurrentLocation.class);
                    startActivity(intent);

                } else {
                    String error = obj.getString("err");
                    toastMsg("Error Message: " + error);
                }
            } else  {
                toastMsg("Network Error, Please Try Again");
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
    /*public void ShowMap(View view){
        final Context context = this;
        Intent intent = new Intent(context, CurrentLocation.class);
        startActivity(intent);
    }*/
}