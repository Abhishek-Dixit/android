package com.example.sangam.chackravuh;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.concurrent.ExecutionException;

public class My_Profile extends AppCompatActivity {
    public static class variable{
        public static int occupation;

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my__profile);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        SharedPreferences sharedpreferences = getSharedPreferences("My_Pref_File", Context.MODE_PRIVATE);
        String phone_sh = sharedpreferences.getString("phone",null);
        String name_sh = sharedpreferences.getString("name",null);
        String loc_sh = sharedpreferences.getString("location",null);
        String occupation_sh =sharedpreferences.getString("occupation",null);
        Log.d("My_Phone", phone_sh);

        if(phone_sh !=null) {
            TextView phone = (TextView) findViewById(R.id.phone_number);
            phone.setText(phone_sh.toString());

        }

        if(name_sh !=null) {
            TextView Name = (TextView) findViewById(R.id.Show_Name);
            Name.setText(name_sh.toString());
            EditText z = (EditText) findViewById(R.id.name);
            z.setText(name_sh.toString());
        }

        if(loc_sh !=null){
            TextView loc = (TextView) findViewById(R.id.my_location);
            loc.setText(loc_sh.toString());

        }
        if(occupation_sh !=null){
            TextView occupation = (TextView) findViewById(R.id.my_occupation);
            occupation.setText(occupation_sh.toString());
        }






    }
    public void check(View view){
        CheckBox doctor = (CheckBox) findViewById(R.id.doctor);
        CheckBox police = (CheckBox) findViewById(R.id.police);
        CheckBox other = (CheckBox) findViewById(R.id.other);
        Log.d("Doctor",doctor.toString());
        switch (view.getId()){
            case R.id.doctor:
                police.setChecked(false);
                other.setChecked(false);
                variable.occupation = 1;
                break;
            case R.id.police:
                doctor.setChecked(false);
                other.setChecked(false);
                variable.occupation = 2;
                break;
            case R.id.other:
                doctor.setChecked(false);
                police.setChecked(false);
                variable.occupation = 3;
                break;


        }
        if(doctor.isChecked()) {
            SharedPreferences sharedpreferences = getSharedPreferences("My_Pref_File", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.putString("occupation","Doctor");
            editor.commit();
        }
        else if(police.isChecked()) {
            SharedPreferences sharedpreferences = getSharedPreferences("My_Pref_File", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.putString("occupation","Police");
            editor.commit();
        }
        else if(other.isChecked()) {
            SharedPreferences sharedpreferences = getSharedPreferences("My_Pref_File", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.putString("occupation","Other");
            editor.commit();
        }

    }

    public void toastMsg(String msg) {

        Toast toast = Toast.makeText(this, msg, Toast.LENGTH_LONG);
        toast.show();

    }

    public void submit(View view){
        EditText z = (EditText) findViewById(R.id.name);
        String my_name = z.getText().toString();

        SharedPreferences sharedpreferences = getSharedPreferences("My_Pref_File", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("name",my_name);
        editor.commit();

        SharedPreferences sharedpref = getSharedPreferences("My_Pref_File", Context.MODE_PRIVATE);
        String name_sh = sharedpref.getString("name",null);
        String occupation_sh =sharedpref.getString("occupation",null);
        String phone_sh = sharedpreferences.getString("phone",null);
        String token_sh = sharedpreferences.getString("token",null);
        if(name_sh != null) {
            TextView Name = (TextView) findViewById(R.id.Show_Name);
            Name.setText(name_sh.toString());
        }
        if(occupation_sh !=null){
            TextView occupation = (TextView) findViewById(R.id.my_occupation);
            occupation.setText(occupation_sh.toString());
        }

        HashMap<String, String> hashMap = new HashMap<String, String>();
        hashMap.put("phone",phone_sh);
        hashMap.put("token",token_sh);
        hashMap.put("name",name_sh);
        hashMap.put("occupation",String.valueOf(variable.occupation));
        //hashMap.put("loc", CurrentLocation.variable.latitude + "," + CurrentLocation.variable.longitude);
        URLDataHash mydata = new URLDataHash();
        mydata.url = "192.168.43.127";
        mydata.apicall = "user/profile";
        mydata.hashMap = hashMap;
        mydata.port = "8092";
        try {
            JSONObject obj = new nodeHttpRequest(this).execute(mydata).get();
            //Log.d("MYAPP", obj.toString());
            if (obj != null) {
                //Log.d("MYAPP", obj.toString());
                //JSONObject resp = obj.getJSONObject("resp");
                String allowed = "success";
                String status = obj.getString("status");
                if (status.equals(allowed)) {
                    toastMsg("Successfully Updated your details");
                } else {
                    String error = obj.getString("err");
                    toastMsg("Error Message: " + error);
                }
                final Context context = this;
                Intent intent = new Intent(context, CurrentLocation.class);
                startActivity(intent);
            } else  {
                toastMsg("Network Error, Please Try Again");
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

}
