package com.example.sangam.chackravuh;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Vibrator;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.concurrent.ExecutionException;

public class CurrentLocation extends FragmentActivity implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    private GoogleMap mMap;
    GoogleApiClient mGoogleApiClient;
    Location mLastLocation;
    Marker mCurrLocationMarker;
    LocationRequest mLocationRequest;

    //Global varibales

    public static class variable{
        public static double longitude;
        public static double latitude;
    }

    public static class pref {
        public static String phone_sh;
        public static String token_sh;
    }

    public static class whereCond {
        public static Boolean helper;
        public static Boolean caller;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_current_location);

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkLocationPermission();
        }
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.

        SharedPreferences sharedpreferences = getSharedPreferences("My_Pref_File", Context.MODE_PRIVATE);
        pref.phone_sh = sharedpreferences.getString("phone",null);
        pref.token_sh = sharedpreferences.getString("token",null);
        Log.d("My_Phone", pref.phone_sh);
        Log.d("My_token", pref.token_sh);


        new Thread(new Runnable(){
            public void run() {
                while(true)
                    try {

                        //Log.d("serv2", "hello")

                        HashMap<String, String> hashMap = new HashMap<String, String>();
                        hashMap.put("phone", pref.phone_sh);
                        hashMap.put("token", pref.token_sh);
                        hashMap.put("lat_long", variable.latitude + "," + variable.longitude);
                        URLDataHash mydata = new URLDataHash();
                        mydata.url = "192.168.1.105";
                        mydata.apicall = "user/update_location";
                        mydata.hashMap = hashMap;
                        mydata.port = "6969";

                        try {
                            JSONObject obj = new nodeHttpRequest(getApplicationContext()).execute(mydata).get();
                            //Log.d("MYAPP", obj.toString());
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        } catch (ExecutionException e) {
                            e.printStackTrace();
                        }
                        Thread.sleep(30000); //30 seconds

                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

            }
        }).start();

        whereCond.helper = true;
        new Thread( new Runnable(){
            public void run() {
                while(whereCond.helper)
                {
                    try {

                        //Log.d("serv2", "hello");
                        Log.d("My_Phone_test", pref.phone_sh);
                        Log.d("My_token_test", pref.token_sh);

                        HashMap<String, String> hashMap = new HashMap<String, String>();
                        hashMap.put("phone",pref.phone_sh);
                        hashMap.put("token",pref.token_sh);
                        URLDataHash mydata = new URLDataHash();
                        mydata.url = "192.168.1.105";
                        mydata.apicall = "user/alert/helper/get_update";
                        mydata.hashMap = hashMap;
                        mydata.port = "9115";

                        try {
                            JSONObject obj = new nodeHttpRequest(getApplicationContext()).execute(mydata).get();
                            if  (obj != null) {
                                //Log.d("MYAPP", obj.toString());
                                String status = obj.getString("status");

                                if (status.equals("success")) {

                                    JSONObject resp = obj.getJSONObject("resp");
                                    String caller_phone = resp.getString("phone");
                                    String caller_name = resp.getString("name");
                                    String caller_loc = resp.getString("loc");

                                    //Open Call page from here and figure out how to send phone, name, location of caller from here to call page

                                    //final Context context = this;
                                    //Intent intent = new Intent(context, CallPage.class);
                                    //startActivity(intent);
                                    whereCond.helper = false;
                                }
                            }
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        } catch (ExecutionException e) {
                            e.printStackTrace();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        Thread.sleep(5000); //5 seconds

                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                }

            }
        }).start();

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);

        //Initialize Google Play Services
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                mMap.setMyLocationEnabled(true);
            }
        }
        else {
            buildGoogleApiClient();
            mMap.setMyLocationEnabled(true);
        }
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(Bundle bundle) {

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10);
        mLocationRequest.setFastestInterval(10);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override

    public void onLocationChanged(Location location) {

        mLastLocation = location;
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }

        //Place current location marker
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        Log.d("My_Loc",latLng.toString());
        variable.longitude = latLng.longitude;
        variable.latitude = latLng.latitude;

        Log.d("My_Longi", String.valueOf(variable.longitude));
        Log.d("My_Lati", String.valueOf(variable.latitude));
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title("Current Position");
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
        mCurrLocationMarker = mMap.addMarker(markerOptions);

        //move map camera
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng,15));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(14),2000,null);

        //stop location updates
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    public boolean checkLocationPermission(){
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Asking user if explanation is needed
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

                //Prompt the user once explanation has been shown
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted. Do the
                    // contacts-related task you need to do.
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        if (mGoogleApiClient == null) {
                            buildGoogleApiClient();
                        }
                        mMap.setMyLocationEnabled(true);
                    }

                } else {

                    // Permission denied, Disable the functionality that depends on this permission.
                    Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
                }
                return;
            }

            // other 'case' lines to check for other permissions this app might request.
            // You can add here other case statements according to your requirement.
        }
    }

    public void toastMsg(String msg) {

        Toast toast = Toast.makeText(this, msg, Toast.LENGTH_LONG);
        toast.show();

    }
    public void help(View view) {

        whereCond.helper = false; //stopping get helper loop for now...

        Log.d("My_Phone", pref.phone_sh);
        Log.d("My_token", pref.token_sh);

        //toastMsg("Dear " + phone_sh + "-Location Updated..!!");
        //toastMsg("Your Location is" + variable.latitude + "," + variable.longitude);
        toastMsg("Stay Calm, We are coming...");

        /*API Request here
        JSON object as parameter:-
                        'phone': numbers,
                        'token': token,
                        'loc': 22.23,27.23
        */
        HashMap<String, String> hashMap = new HashMap<String, String>();
        hashMap.put("phone",pref.phone_sh);
        hashMap.put("token",pref.token_sh);
        hashMap.put("loc",variable.latitude + "," + variable.longitude);
        //send type also
        URLDataHash mydata = new URLDataHash();
        mydata.url = "192.168.1.105";
        mydata.apicall = "user/alert/caller/create";
        mydata.hashMap = hashMap;
        mydata.port = "9114";

        try {
            JSONObject obj = new nodeHttpRequest(getApplicationContext()).execute(mydata).get();
            if (obj != null) {

                Log.d("MYAPP", obj.toString());
                String allowed = "success";
                String status = obj.getString("status");
                if(status.equals(allowed)){
                    toastMsg("Your Request is initiated, Please Wait for Responses...");

                    whereCond.caller = true;
                    new Thread(new Runnable(){
                        public void run() {
                            while(whereCond.caller)
                            {
                                try {

                                    //Log.d("serv2", "hello");
                                    Log.d("My_Phone_test", pref.phone_sh);
                                    Log.d("My_token_test", pref.token_sh);

                                    HashMap<String, String> hashMap = new HashMap<String, String>();
                                    hashMap.put("phone",pref.phone_sh);
                                    hashMap.put("token",pref.token_sh);
                                    URLDataHash mydata = new URLDataHash();
                                    mydata.url = "192.168.1.105";
                                    mydata.apicall = "user/alert/caller/get";
                                    mydata.hashMap = hashMap;
                                    mydata.port = "9114";

                                    try {
                                        JSONObject obj = new nodeHttpRequest(getApplicationContext()).execute(mydata).get();
                                        if  (obj != null) {
                                            //Log.d("MYAPP", obj.toString());
                                            String status = obj.getString("status");

                                            if (status.equals("success")) {

                                                JSONArray resp = obj.getJSONArray("resp"); //Here resp will be an array of objects containing helper info
                                                if (resp.length() > 0) {
                                                    for(int i = 0; i < resp.length(); i++) {
                                                        JSONObject helper = resp.getJSONObject(i);
                                                        String helper_phone = helper.getString("phone");
                                                        String helper_name = helper.getString("name");
                                                        String helper_loc = helper.getString("loc");
                                                        String type = helper.getString("type");
                                                        Log.d("helper_phone", helper_phone);
                                                    }
                                                } else {
                                                    whereCond.caller = false; //close thread if all helpers have responded
                                                }

                                                //Open helper details page from here and figure out a way to send all the data there

                                                //final Context context = this;
                                                //Intent intent = new Intent(context, HelperPage.class);
                                                //startActivity(intent);
                                            }
                                        } else {
                                            toastMsg("Keep Calm we're still trying to get somebody for you.");
                                        }
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    } catch (ExecutionException e) {
                                        e.printStackTrace();
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    Thread.sleep(5000); //5 seconds

                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }

                            }

                        }
                    }).start();
                }
                else {
                    String error = obj.getString("err");
                    toastMsg("Error Message: " + error);
                }
            } else {
                toastMsg("Network Error, Please Try Again");
            }

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        // Fake location coordinates
        /*MarkerOptions markerOptions = new MarkerOptions();
        //Active Helper 1
        LatLng helper1 = new LatLng(variable.latitude + 0.004025,variable.longitude + 0.005023);
        markerOptions.position(helper1);
        markerOptions.title("Active Helper 1");
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
        mCurrLocationMarker = mMap.addMarker(markerOptions);

        //Active Helper 2
        LatLng helper2 = new LatLng(variable.latitude - 0.003016,variable.longitude + 0.005034);
        markerOptions.position(helper2);
        markerOptions.title("Active Helper 2");
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
        mCurrLocationMarker = mMap.addMarker(markerOptions);

        //Active Helper 3
        LatLng helper3 = new LatLng(variable.latitude + 0.003043,variable.longitude - 0.004054);
        markerOptions.position(helper3);
        markerOptions.title("Active Helper 3");
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
        mCurrLocationMarker = mMap.addMarker(markerOptions);

    */

        // Get instance of Vibrator from current Context
        Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        // Vibrate for 400 milliseconds
        v.vibrate(400);

    }

    public void GetDetails(View view){
        final Context context = this;
        Intent intent = new Intent(context, My_Profile.class);
        startActivity(intent);
    }
}
